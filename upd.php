<?php

	# Setear conexion a la DB
	require 'db.php';

	# Control de contenido recibido
	if(!$_SERVER['QUERY_STRING']) {
		echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido contenido']); die;
	} else {
		# Separar todos los contenidos
		$get_content = explode('&', $_SERVER['QUERY_STRING']);

		# Preparar array para el contenido
		$info = array();

		# Cargar el array con el contenido recibido
		foreach ($get_content as $item) {
			$temp = explode('=', $item);
			$info[$temp[0]] = str_replace('%20', ' ', $temp[1]);
		}

		# Control del token de seguridad (existencia y valor)
		if(!array_key_exists('sec_code', $info)) { echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido el token de seguridad (sec_code)']); die; }
		if($info['sec_code'] != 'uamericana') { echo json_encode(['status' => 'error', 'msg' => 'Token no valido']); die; }

		# Control de recepción del contenido
		if(!array_key_exists('nombre', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese el nombre del cliente']); die; }
		if(!array_key_exists('apellido', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese el apellido del cliente']); die; }
		if(!array_key_exists('cedula', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese la cedula del cliente']); die; }
		if(!array_key_exists('correo', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese el correo del cliente']); die; }
		if(!array_key_exists('telefono', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese el telefono del cliente']); die; }

		try {
			$sql = " UPDATE clientes SET
				nombre		 = :nom,
				apellido	 = :ape,
				correo		 = :cor,
				telefono	 = :tel
				WHERE cedula = :ced ";
			$update = $mysql->prepare($sql);
			$update->execute([ ':nom' => $info['nombre'], ':ape' => $info['apellido'], ':ced' => $info['cedula'], ':cor' => $info['correo'], ':tel' => $info['telefono'] ]);

			echo json_encode(['status' => 'success', 'msg' => 'El cliente se ha actualizado correctamente']); die;

		} catch(PDOException $e) {
			echo json_encode($e->getMessage()) ;die;
		}
	}
?>