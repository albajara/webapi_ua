<?php echo json_encode(['status' => 'error', 'msg' => 'Aqui no hay nada...']) ?>

<!--

	FICHERO		|	ACCION
	-------------------------------------------------------
	ins.php		|	registro de cliente
	upd.php		|	actualizacion de cliente
	del.php		|	eliminar cliente (por numero de cedula)
	vw.php		|	ver JSON de clientes

	[ Para realizar desde el navegador -- Puede realizarse tambien por otros medios]
	alba-ua.ml/ins.php?nombre=juan%20rodrigo&apellido=lopez%20amarilla&correo=j.lopez@dominio.com&telefono=0981123456&cedula=1234562&sec_code=uamericana
	alba-ua.ml/upd.php?nombre=juan%20rodrigo&apellido=lopez%20amarilla&correo=j.lopez@dominio.com&telefono=0981123456&cedula=1234562&sec_code=uamericana
	alba-ua.ml/del.php?cedula=1234562
	alba-ua.ml/vw.php?sec_code=uamericana

	==> Campos Obligatorios
		· registro		: nombre, apellido, correo, telefono, cedula, sec_code
		· actualización	: nombre, apellido, correo, telefono, cedula, sec_code
		· eliminación	: cedula, sec_code
		· visualizacion	: sec_code

-->