<?php

	# Setear conexion a la DB
	require 'db.php';

	# Control de contenido recibido
	if(!$_SERVER['QUERY_STRING']) {
		echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido contenido']); die;
	} else {
		# Separar todos los contenidos
		$get_content = explode('&', $_SERVER['QUERY_STRING']);

		# Preparar array para el contenido
		$info = array();

		# Cargar el array con el contenido recibido
		foreach ($get_content as $item) {
			$temp = explode('=', $item);
			$info[$temp[0]] = str_replace('%20', ' ', $temp[1]);
		}

		# Control del token de seguridad (existencia y valor)
		if(!array_key_exists('sec_code', $info)) { echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido el token de seguridad (sec_code)']); die; }
		if($info['sec_code'] != 'uamericana') { echo json_encode(['status' => 'error', 'msg' => 'Token no valido']); die; }

		# Control de recepción del contenido
		if(!array_key_exists('cedula', $info)) { echo json_encode(['status' => 'error', 'msg' => 'Ingrese la cedula del cliente']); die; }

		try {
			# Control de existencia de cliente
			$sql = " SELECT * FROM clientes WHERE cedula = :ced ";
			$query = $mysql->prepare($sql);
			$query->execute([ ':ced' => $info['cedula'] ]);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$rst = $query->fetch();

			if(empty($rst)) {
				# Al no existir cliente, se retorna mensaje
				echo json_encode(['status' => 'error', 'msg' => 'No se ha encontrado cliente con el numero de cedula indicado']); die;
			}

			# En caso de existir cliente, se elimina de la base de datos
			$sql = " DELETE FROM clientes WHERE cedula = :ced ";
			$update = $mysql->prepare($sql);
			$update->execute([ ':ced' => $info['cedula'] ]);

			echo json_encode(['status' => 'success', 'msg' => 'El cliente se ha eliminado correctamente']); die;

		} catch(PDOException $e) {
			echo json_encode($e->getMessage()) ;die;
		}
	}
?>