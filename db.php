<?php

	try {
		define('db_server'	, '');
		define('db_name'	, '');
		define('db_user'	, '');
		define('db_pass'	, '');

		$mysql  =   new PDO('mysql:host='. db_server .';dbname='. db_name , db_user , db_pass);
		$mysql  ->  setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo $e->getMessage();
		exit;
	}

/*	Estructura de la base de datos

			CREATE TABLE `clientes` (
				`nombre` VARCHAR(75) NOT NULL,
				`apellido` VARCHAR(75) NOT NULL,
				`cedula` VARCHAR(20) NOT NULL,
				`correo` VARCHAR(100) NOT NULL,
				`telefono` VARCHAR(10) NOT NULL,
				`fecha_registro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
				PRIMARY KEY (`cedula`)
			) ENGINE=InnoDB;
*/

?>