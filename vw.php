<?php

	# Setear conexion a la DB
	require 'db.php';

	# Control de contenido recibido
	if(!$_SERVER['QUERY_STRING']) {
		echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido contenido']); die;
	} else {
		# Separar todos los contenidos
		$get_content = explode('&', $_SERVER['QUERY_STRING']);

		# Preparar array para el contenido
		$info = array();

		# Cargar el array con el contenido recibido
		foreach ($get_content as $item) {
			$temp = explode('=', $item);
			$info[$temp[0]] = str_replace('%20', ' ', $temp[1]);
		}

		# Control del token de seguridad (existencia y valor)
		if(!array_key_exists('sec_code', $info)) { echo json_encode(['status' => 'error', 'msg' => 'No se ha recibido el token de seguridad (sec_code)']); die; }
		if($info['sec_code'] != 'uamericana') { echo json_encode(['status' => 'error', 'msg' => 'Token no valido']); die; }

		try {
			# Obtener todos los registros
			$sql = " SELECT * FROM clientes ORDER BY fecha_registro ASC ";
			$select = $mysql->prepare($sql);
			$select->execute();
			$select->setFetchMode(PDO::FETCH_ASSOC);

			# Ordenar el contenido recibido
			$return = array();
			while($rst = $select->fetch()) {
				$return[] = $rst;
			}

			# Formatear como JSON para retornar todo el contenido
			echo json_encode($return); die;

		} catch(PDOException $e) {
			echo json_encode($e->getMessage()) ;die;
		}
	}
?>